package migration.core.utils;

import com.day.cq.dam.api.Asset;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhernandez on 2017-11-20.
 */
public class MigrationUtil {

    public MigrationUtil(){

    }

    public List<String> readXMLFile(Resource resource){
        List<String> results = new ArrayList<String>();

        try {
            Asset asset = resource.adaptTo(Asset.class);

            DocumentBuilderFactory docFactory =  DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = null;
            doc = docBuilder.parse(asset.getOriginal().getStream());

            Element exportSettings = doc.getDocumentElement();
            NodeList exportObject = exportSettings.getChildNodes();
            Element deploymentObjects = (Element) exportObject.item(1);
            NodeList urls = deploymentObjects.getChildNodes();

            for (int i = 0; i < urls.getLength(); i++) {
                org.w3c.dom.Node nNode = urls.item(i);
                if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    Element deploymentObject = (Element) urls.item(i);
                    String originUrl = deploymentObject.getAttribute("Url");
                    results.add(originUrl);
                }
            }

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return results;
    }

    public void createFile(String filename, String extension, String content) {
        try {
            FileWriter fw = new FileWriter("migration/" +filename + extension);
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public String cleanSPPagesConvention(String url){
        String result = null;

        if (url.startsWith("/Pages/Default.aspx") || url.startsWith("/Pages/default.aspx")) {
            result = "/Home";
        }else if ( url.contains("/Pages/Default.aspx") || url.contains("/Pages/default.aspx") || url.contains("/Pages") ) {
            result = url.replaceAll("/Pages/[D|d]efault.aspx|/Pages/[D|d]efault.aspx|/Pages","");
        }


        return result;
    }

    public String replaceASPXExtension(String url) {
        String removeExt = null;
        if ( url.contains(".aspx")) {
            removeExt = url.substring(0,url.lastIndexOf(".aspx"));
        } else {
            removeExt = url;
        }
        return removeExt;
    }

    public String changeSpecialCharacters(String url){
        String newUrl = url;

        if ( url.contains("’")) {
            String replace = url.replace("’","");
            newUrl = replace;
        }

        if ( url.contains("®")) {
            String replace = url.replace("®","");
            newUrl = replace;
        }

        if ( url.contains("–")) {
            String replace = url.replace("–","-");
            newUrl = replace;
        }

        if ( url.contains("“") || url.contains("“")) {
            String replace = url.replace("“","-");
            newUrl = replace.replace("”","-");
        }

        if ( url.contains("‘") || url.contains("’")) {
            String replace = url.replaceAll("‘","-");
            newUrl = replace.replace("’","-");
        }

        if ( url.contains("ë")) {
            String replace = url.replace("ë","e");
            newUrl = replace;
        }

        if ( url.contains("”")) {
            String replace = url.replace("”","");
            newUrl = replace;
        }

        if ( url.contains("—")) {
            String replace = url.replace("—","-");
            newUrl = replace;
        }

        if ( url.contains("…")) {
            String replace = url.replace("…","-");
            newUrl = replace;
        }

        if ( url.contains(".")) {
            String replace = url.replace(".","_");
            newUrl = replace;
        }

        if ( url.contains(" ")) {
            String replace = url.replace(" ","-");
            newUrl = replace;
        }

        return newUrl;
    }

}
