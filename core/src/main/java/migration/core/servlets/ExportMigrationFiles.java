package migration.core.servlets;

import migration.core.services.MigrationServiceImpl;
import migration.core.services.MigrationServiceInterface;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by jhernandez on 2017-11-20.
 */
@Component(service=Servlet.class,
        property={
                Constants.SERVICE_DESCRIPTION + "=Export migration files. Create CSV files from ExportSettings.xml",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths="+ "/bin/custom/exportfiles.html"
        })
public class ExportMigrationFiles extends SlingSafeMethodsServlet {

    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException {
        ResourceResolver resourceResolver = req.getResourceResolver();
        PrintWriter wr = resp.getWriter();
        String path = "/content/dam/migration_content/origin/ExportSettings3.xml";

        MigrationServiceInterface service = new MigrationServiceImpl(resourceResolver);

        List<String> files = service.migrationFiles(path);

        wr.write("Total URL's to migrate: " + files.size() + "\r");

        int ind = 1;
        for ( String file : files ) {
            wr.write("[" + ind + "]\t" + file + "\r");
            ind++;
        }
    }

}
