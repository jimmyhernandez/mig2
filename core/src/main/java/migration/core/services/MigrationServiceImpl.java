package migration.core.services;

import migration.core.utils.MigrationUtil;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.List;

/**
 * Created by jhernandez on 2017-11-20.
 */
public class MigrationServiceImpl implements MigrationServiceInterface {
    private ResourceResolver _resourceResolver = null;
    private MigrationUtil _migrationUtil = null;

    public MigrationServiceImpl(ResourceResolver resourceResolver){
        if ( _resourceResolver == null ) {
            _resourceResolver = resourceResolver;
        }

        if ( _migrationUtil == null ) {
            _migrationUtil = new MigrationUtil();
        }
    }

    public List<String> migrationFiles(String path) {
        Resource resource = _resourceResolver.getResource(path);
        List<String> urls = null;
        if ( resource != null ) {
            urls =  _migrationUtil.readXMLFile(resource);
            this.createMigrationCSVFiles(urls);
            return urls;
        }
        return null;
    }

    public void createMigrationCSVFiles(List<String> urls) {
        int fileNumber = 1;
        int count = 1;
        String extension = ".csv";
        String header = "id\torigin_url\tsp2aem\tfile_name\r";
        StringBuilder oneRow = new StringBuilder();
        oneRow.append(header);

        for ( String url : urls) {
            String originUrl = url;
            if ( (oneRow.toString().getBytes().length > 10500 && oneRow.toString().getBytes().length < 11000)) {
                _migrationUtil.createFile("migration" + fileNumber,extension,oneRow.toString());
                fileNumber++;
                oneRow = new StringBuilder();
                oneRow.append(header);
            }
            //Create an Id
            oneRow.append(count + "\t");
            //Origin URL
            oneRow.append(url + "\t");
            //Remove /Pages from URL
            String sp2AEMPath = _migrationUtil.cleanSPPagesConvention(originUrl);
            sp2AEMPath = _migrationUtil.replaceASPXExtension(sp2AEMPath);

            String fileName = sp2AEMPath.substring(sp2AEMPath.lastIndexOf("/") + 1);

            sp2AEMPath = _migrationUtil.changeSpecialCharacters(sp2AEMPath);
            oneRow.append(sp2AEMPath+"\t");

            oneRow.append(fileName+"\r");

            //Destination path AEM
            //String aemContentPath = contentUtil.getAEMContentPath(sp2AEMPath);
            //String removedSpecialCharacters = contentUtil.changeSpecialCharacters(aemContentPath);
            //oneRow.append(removedSpecialCharacters + "\r");


            if ( count == urls.size() && oneRow.toString().getBytes().length <= 10500 ) {
                _migrationUtil.createFile("migration" + fileNumber,extension,oneRow.toString());
                break;
            }

            count++;
        }
    }
}
