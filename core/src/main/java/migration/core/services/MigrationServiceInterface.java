package migration.core.services;

import java.util.List;

/**
 * Created by jhernandez on 2017-11-20.
 */
public interface MigrationServiceInterface {
    List<String> migrationFiles(String path);

}
